﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.FtpClient;

namespace BackupFTP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private FtpClient conn;
        private void button1_Click(object sender, EventArgs e)
        {
            using (conn = new FtpClient()) 
            {
                Connect(conn);

            }
        }

        public void Connect(FtpClient conn)
        {
            conn.Host = textBox1.Text;
            conn.Credentials = new NetworkCredential(textBox2.Text, textBox3.Text);
            conn.Connect();
        }

        public void GetListing()
        {
            foreach (FtpListItem item in conn.GetListing(conn.GetWorkingDirectory(),
                    FtpListOption.Modify | FtpListOption.Size))
                {

                    switch (item.Type)
                    {
                        case FtpFileSystemObjectType.Directory:
                            break;
                        case FtpFileSystemObjectType.File:
                            break;
                        case FtpFileSystemObjectType.Link:
                            // derefernece symbolic links
                            if (item.LinkTarget != null)
                            {
                                // see the DereferenceLink() example
                                // for more details about resolving links.
                                item.LinkObject = conn.DereferenceLink(item);

                                if (item.LinkObject != null)
                                {
                                    // switch (item.LinkObject.Type)...
                                }
                            }
                            break;
                    }
                }

                // same example except automatically dereference symbolic links.
                // see the DereferenceLink() example for more details about resolving links.
                foreach (FtpListItem item in conn.GetListing(conn.GetWorkingDirectory(),
                    FtpListOption.Modify | FtpListOption.Size | FtpListOption.DerefLinks))
                {

                    switch (item.Type)
                    {
                        case FtpFileSystemObjectType.Directory:
                            break;
                        case FtpFileSystemObjectType.File:
                            break;
                        case FtpFileSystemObjectType.Link:
                            if (item.LinkObject != null)
                            {
                                // switch (item.LinkObject.Type)...
                            }
                            break;
                    }
                }
        }
    }
}
